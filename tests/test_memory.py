from datetime import datetime, timedelta

from langchain.chat_models.openai import ChatOpenAI

from memo import Memory, Retriever


def test_memory(create_retriever):
    retriever: Retriever = create_retriever

    current = datetime.now()

    memory = Memory(
        llm=ChatOpenAI(client=None),
        memory_retriever=retriever,
        verbose=False,
        reflection_threshold=8,
    )

    memory.add_memory("My name is Homer", now=current - timedelta(days=3))
    memory.add_memory("My name is Bart", now=current - timedelta(days=2))
    memory.add_memory("My name is Maggie", now=current - timedelta(days=1))

    docs = memory.fetch_memories("What is my name?")
    assert len(docs) == 3

    memory.add_memory("I like playing Mario")
    docs = memory.fetch_memories("What is my favorite game?")
    assert len(docs) == 4
    assert "Mario" in docs[0].page_content
