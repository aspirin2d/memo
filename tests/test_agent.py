from langchain.chat_models import ChatOpenAI

from memo import Agent, Memory, Retriever


def test_agent(create_retriever):
    retriever: Retriever = create_retriever
    llm = ChatOpenAI(client=None)

    memory = Memory(
        llm=llm,
        memory_retriever=retriever,
        verbose=False,
        reflection_threshold=8,
    )

    homer = Agent(
        name="Homer",
        traits="lazy, ignorant, has many flaws, loves his family",
        status="",
        memory=memory,
        llm=llm,
    )
    homer_observations = [
        "Hommer is the husband of Marge Simpson and father of "
        + "Bart, Lisa and Maggie Simpson. "
        "Homer's favorite color is blue",
        "Homer enjoys dancing, eating donuts, dancing, drinking",
        "Homer works as a low-level safety inspector at "
        + "the Springfield Nuclear Power Plant",
    ]

    for obs in homer_observations:
        print("add memory: ", obs)
        homer.memory.add_memory(obs)

    not_finished, answer = homer.generate_dialogue_response(
        "Aspirin says: 'what is your favorite color?'"
    )
    assert not_finished is True
    assert "blue" in answer

    not_finished, answer = homer.generate_dialogue_response(
        "Aspirin says: 'My favorite color is brown,"
        + " because I love drinking coffee. Goodbye!'"
    )
    assert not_finished is False
    print(answer)

    not_finished, answer = homer.generate_dialogue_response(
        "Aspirin says: 'If you want to buy a hat as my birthday gift, "
        + "what color would you choose?'"
    )
    print(answer)
