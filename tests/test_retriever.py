from datetime import datetime, timedelta

from langchain.docstore.document import Document

from memo import Retriever


def test_retriever(create_retriever):
    retriever: Retriever = create_retriever

    current = datetime.now()

    retriever.add_documents(
        [Document(page_content="My name is Homer")],
        current_time=current - timedelta(days=2),  # two days ago
    )

    retriever.add_documents(
        [Document(page_content="My name is Bart")],
        current_time=current - timedelta(days=1),  # yesterday
    )

    retriever.add_documents(
        [Document(page_content="My name is Maggie")],
        current_time=current,
    )

    # time sensitive
    result = retriever.get_relevant_documents("What's my name?")
    assert "Maggie" in result[0].page_content
