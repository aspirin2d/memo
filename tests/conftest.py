import os

import dotenv
import pytest
from langchain.chat_models import ChatOpenAI
from langchain.embeddings import OpenAIEmbeddings
from qdrant_client import QdrantClient
from qdrant_client.models import Distance, VectorParams

from memo import Retriever, Store

dotenv.load_dotenv(".env")

collection_name = "test_connection"
llm = ChatOpenAI(client=None)


@pytest.fixture(scope="session")
def connect_qdrant():
    url = os.getenv("QDRANT_URL")
    assert url is not None

    qdrant = QdrantClient(url=url, prefer_grpc=True)
    return qdrant


@pytest.fixture(scope="function")
def create_store(connect_qdrant):
    client: QdrantClient = connect_qdrant

    # create a new collection
    client.recreate_collection(
        collection_name=collection_name,
        vectors_config=VectorParams(size=1536, distance=Distance.COSINE),
    )
    store = Store(
        client=client,
        collection_name=collection_name,
        embeddings=OpenAIEmbeddings(client=None),
    )
    yield store

    deleted = client.delete_collection(collection_name=collection_name)
    assert deleted is True


@pytest.fixture(scope="function")
def create_retriever(create_store):
    store: Store = create_store
    retriever = Retriever(vectorstore=store, other_score_keys=["imptratance"], k=15)
    return retriever
