from memo.utils import agent_from_toml


def test_from_toml():
    agent = agent_from_toml("./examples/config/homer.toml")
    assert agent is not None

    _, answer = agent.generate_dialogue_response(
        "Aspirin says: what's your favorite color?"
    )
    assert "blue" in answer.lower()

    with open('./examples/config/homer.toml', mode="rb") as fp:
        agent = agent_from_toml(fp)
        assert agent is not None

        _, answer = agent.generate_dialogue_response(
            "Aspirin says: what's your favorite color?"
        )
        assert "blue" in answer.lower()
