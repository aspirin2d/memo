.PHONY: all clean format lint test tests test_watch integration_tests docker_tests help extended_tests

all: help

format:
	poetry run black .
	poetry run ruff --fix .

PYTHON_FILES=.
lint: PYTHON_FILES=.
lint_diff: PYTHON_FILES=$(shell git diff --name-only --diff-filter=d master | grep -E '\.py$$')

lint lint_diff:
	poetry run black $(PYTHON_FILES) --check
	poetry run ruff .

TEST_FILE ?= .

test:
	poetry run pytest ${TEST_FILE}

docker-build:
	docker build -t my-memo .

help:
	@echo '----'
	@echo 'format                       - run code formatters'
	@echo 'lint                         - run linters'
	@echo 'test                         - run unit tests'
	@echo 'tests                        - run unit tests'
	@echo 'test TEST_FILE=<test_file>   - run all tests in file'
	@echo 'docker-build                 - build Dockerfile'
