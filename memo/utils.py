import os
import tomllib
from io import BufferedReader
from typing import BinaryIO
from uuid import uuid4

from fastapi import UploadFile
from langchain.chat_models.openai import ChatOpenAI
from langchain.embeddings import OpenAIEmbeddings
from pydantic import parse_obj_as
from qdrant_client import QdrantClient
from qdrant_client.models import Distance, VectorParams

from memo import Agent, Memory, Retriever, Store


def agent_from_toml(file: str | BufferedReader | UploadFile | BinaryIO) -> Agent:
    """create some agents from a toml file"""
    if type(file) is str:
        with open(file, mode="rb") as fp:
            agent_src = tomllib.load(fp)
    elif isinstance(file, BufferedReader):
        agent_src = tomllib.load(file)
    elif isinstance(file, UploadFile):
        agent_src = tomllib.load(file.file)
    else:
        raise ValueError("file type not supported")

    # create a collection_name by config's id
    if agent_src.get("id") is None:
        collection_name: str = uuid4().hex
    else:
        collection_name: str = str(agent_src.get("id"))

    # connect to qdrant
    qclient = QdrantClient(url=os.getenv("QDRANT_URL"), prefer_grpc=True)
    qclient.recreate_collection(
        collection_name=collection_name,
        vectors_config=VectorParams(size=1536, distance=Distance.COSINE),
    )

    # create qdrant store
    store = Store(
        client=qclient,
        collection_name=collection_name,
        embeddings=OpenAIEmbeddings(client=None),
    )

    # create retriever from store
    retriever = Retriever(vectorstore=store, other_score_keys=["imptratance"], k=15)
    # create memory from retriever
    llm = ChatOpenAI(client=None)
    memory = Memory(llm=llm, memory_retriever=retriever)

    # add memories from toml
    for mem in agent_src["memories"]:
        memory.add_memory(mem)

    # create agent at last
    agent_src |= {"memory": memory, "llm": llm}
    agent = parse_obj_as(Agent, agent_src)

    return agent
