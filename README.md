# MEMO

**MEMO**，一款记忆驱动的AI互动平台。

## 本地安装

1. 在windows上安装Dockers，安装方法请看[这里](https://docs.docker.com/desktop/install/windows-install/)

1. 克隆项目: `git clone https://gitlab.q1op.com/codez/gptplatform/memo.git`， 并进入该目录 `cd memo`

1. 确保你的环境是在代理模式下的：`export https_proxy=127.0.0.1:7980`，因为每个代理软件的端口不同，请自行修改。

1. 在`docker_files`文件夹下面创建`.env`文件，并修改以下内容：
```
QDRANT_URL=http://qdrant
OPENAI_API_KEY=sk-你的OPENAI API KEY
```

1. 运行docker compose: `docker compose -f ./docker_files/docker-compose.yaml`

1. 打开浏览器输入下面的网址: `http://localhost:8888`，打开examples目录下的`agent.ipynb`

1. 接下来按照打开的文档一步一步进行操作就可以了。关于jupyter的使用方法，可以参考[这里](https://jupyter-notebook.readthedocs.io/en/latest/notebook.html)。
